"""
Database and query interface for raw API usage examples from a corpus
of programs.
"""

from abc import ABC, abstractmethod
from dataclasses import dataclass
from pathlib import Path
from random import Random
from typing import Dict, List
from compress_pickle import loads
from kitchensink.utility.simplified_asts import SimplifiedAST
from kitchensink.utility.zodb import DatabasePool


@dataclass(eq=True, frozen=True)
class DatabaseExamplesConfig:
    """
    configuration POD with fields controlling the database search
    """

    k: int = 3  # number of examples to return
    samples: int = 1000  # number of examples to sample from
    min_context: int = 3  # min number of lines in the example
    seed: int = 0  # seed for the random number generator


class SimplifiedASTsDB(ABC):
    """
    represents a collection of raw simplified ASTs with an interface to query
    for those ASTs containing calls to a given API
    """

    def query(
        self,
        language: str,
        module: str,
        function: str,
        config: DatabaseExamplesConfig = DatabaseExamplesConfig(),
    ) -> List[SimplifiedAST]:
        """
        query the corpora for raw examples of the usage of a particular API

        :param language: source code language of the API function
        :param module: module containing the API function
        :param function: name of the API function
        :param config: configuration constants for the database search
        """

        def matches(token: str, trees: List[SimplifiedAST]) -> List[SimplifiedAST]:
            """
            return a list of subtrees of @trees matching @token

            :param token: string to match
            :param trees: list of simplified ASTs
            """
            results = []

            for tree in trees:
                for subtree in tree.traverse():
                    if token == subtree.token_or_label():
                        results.append(subtree)
                        break

            return results

        def context(
            node: SimplifiedAST,
            config: DatabaseExamplesConfig = config,
        ) -> SimplifiedAST:
            """
            return the parent of @node which has sufficient context around
            the API usage

            :param node: node matching the function to find API usage for
            :param config: configuration constants defining the minimum
                amount of context around the API usage
            """
            result = node

            while result.parent and len(result.text().split("\n")) < config.min_context:
                result = result.parent

            return result

        def weight(
            tree: SimplifiedAST,
            total_samples: int,
            config: DatabaseExamplesConfig = config,
        ) -> float:
            """
            return a weight defining the probability we should select this tree
            to utilize as an API usage example

            :param tree: simplified AST tree with the requested API usage
            :param total_samples: total number of tree containing the API usage
            :param config: configuration constants defining the minimum
                amount of context around the API usage
            """
            denominator = max(len(tree.text().split("\n")), config.min_context)
            denominator *= max(total_samples, 1)
            return 1.0 / denominator

        # Global data structures for the query.
        r = Random(config.seed)

        # Query the database for example usages of the function.
        trees = self._database_query(
            language,
            module,
            function,
            config,
        )

        # Find the subtrees which match the given function name (1).
        # Once found, retrieve the parent ASTs which provide adequate
        # context for the function usage (2).  Compute a set of weights
        # to guide the selection of @k example trees, biasing the
        # selection slightly towards smaller, and thus, more comprehensible,
        # examples (3).  Once selected, return the example# usages (4).
        trees = matches(function, trees)  # (1)
        trees = [context(tree, config) for tree in trees]  # (2)
        weights = [weight(tree, len(trees), config) for tree in trees]  # (3)
        trees = r.choices(trees, weights=weights, k=config.k) if trees else []  # (3)
        return trees  # (4)

    @abstractmethod
    def _database_query(
        self,
        language: str,
        module: str,
        function: str,
        config: DatabaseExamplesConfig = DatabaseExamplesConfig(),
    ) -> List[SimplifiedAST]:
        """
        return an unprocessed list of simplified ASTs containing calls
        to the given API from the database

        :param language: source code language of the API
        :param module: module containing the API function
        :param function: name of the API function
        :param config: configuration constants for the database search
        """
        raise NotImplementedError


class InMemorySimplifiedASTsDB(SimplifiedASTsDB):
    """
    database where examples are stored in memory
    """

    def __init__(self, data: Dict = {}):
        self.data = data

    def _database_query(
        self,
        language: str,
        module: str,
        function: str,
        config: DatabaseExamplesConfig = DatabaseExamplesConfig(),
    ) -> List[SimplifiedAST]:
        r = Random(config.seed)
        trees = self.data.get(language, {}).get(module, {}).get(function, [])
        trees = r.sample(trees, min(config.k, len(trees)))
        return trees


class ZODBSimplifiedASTsDB(SimplifiedASTsDB):
    """
    database where examples are stored in a ZODB instance
    """

    DEFAULT_PATH = Path(__file__).parent / "resources" / "simplified_asts.db"
    TREES_KEY = "trees"
    REFS_KEY = "refs"

    def __init__(self, db_path: Path = DEFAULT_PATH):
        if not db_path.exists():
            raise FileNotFoundError(f"ZODB at {db_path} does not exist.")

        self.database = DatabasePool.pool_create_database(db_path)

    def _database_query(
        self,
        language: str,
        module: str,
        function: str,
        config: DatabaseExamplesConfig = DatabaseExamplesConfig(),
    ) -> List[SimplifiedAST]:
        with self.database.transaction() as conn:
            # Retrieve references to those simplified AST trees containing
            # calls to the given module/function.
            root = conn.root()
            refs = root.get(ZODBSimplifiedASTsDB.REFS_KEY, {})
            refs = refs.get(language, {})
            refs = refs.get(module, {})
            refs = refs.get(function, [])

            # Having collected the path:function references to trees containing
            # calls to the given module/function, retrieve the serialized
            # simplified ASTs themselves.
            trees = []
            for path in refs:
                for function in refs[path]:
                    bits = root[ZODBSimplifiedASTsDB.TREES_KEY][path][function]
                    trees.append(bits)

            # Limit the to K sampled trees.
            r = Random(config.seed)
            trees = r.sample(trees, min(config.k, len(trees)))

            # Deserialize the trees before returning them.
            trees = [loads(tree, compression="gzip") for tree in trees]
            trees = [SimplifiedAST.from_json(tree) for tree in trees]

            return trees
