import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="snippet_mining",
    author="Grammatech",
    description="Collection of techniques to present example code snippets to developers.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/snippet-mining",
    packages=[
        "snippet_mining.cmd",
        "snippet_mining.lib",
    ],
    package_dir={"": "src"},
    python_requires=">=3.6",
    classifiers=[
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    package_data={
        "snippet_mining.cmd": ["schemas/*.json"],
        "snippet_mining.lib": [
            "resources/simplified_asts.db/*.sqlite3",
            "resources/exempla_gratis.db/*.sqlite3",
        ],
    },
    entry_points={
        "console_scripts": [
            "snippet-mining-server = snippet_mining.cmd.server:main",
        ]
    },
    install_requires=[
        "asts",
        "compress_pickle",
        "dpcontracts",
        "flask",
        "flask_restful",
        "jedi",
        "jsonschema",
        "markdown-strings",
        "pygls",
        "zodb",
        "RelStorage[sqlite3]",
        "kitchensink @ git+https://gitlab.com/GrammaTech/Mnemosyne/learn-synth/kitchensink@master",
    ],
    extras_require={"dev": ["black", "flake8", "pre-commit"]},
)
